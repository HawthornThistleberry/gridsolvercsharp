# GridSolver #

This is a C# application for Visual Studio 2012. You should be able to simply open the solution file and build. Alternately, create a new console application and paste the code from Program.cs into it, nothing else has been customized.

### The application ###

I had a job application test which posed a question in which you write a program to solve a "Boggle"-type puzzle. Given a random, or specified, grid of four by four letters, find all the words you can form by moving from tile to tile in any direction, including diagonally, not reusing any tile. This is my working and optimized solution which finds the list in a minute or so. It's a console application so it can spit out the output (which you could run through sort or uniq if you like) and to focus on the problem and solution, not Windows stuff.