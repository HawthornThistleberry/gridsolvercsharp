﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*
 * This is a C# program (written and tested in Visual Studio 2012) to solve a grid puzzle
 * posed by SumOfUs on a job application test. They only gave a limited amount of time for
 * writing the algorithm but allowed it to be in any modern object-oriented language.
 * I only had about 50 minutes when I got to it, so I submitted psuedocode that showed
 * that I understood the problem and could identify the assumptions that I had to make,
 * and conceptualize and implement a working solution, without actually dealing with the
 * vagaries of any particular language. (Perhaps this was a mistake and some languages
 * might have allowed me to actually solve it in the alloted time.) Afterwards, I felt
 * compelled to test my solution by implementing it in something, and I happened to be
 * working with C# so I went ahead and implemented it there. (I also started doing it in
 * Ruby on Rails, but the web-centric nature of Ruby on Rails didn't seem as good a fit
 * to this problem.)
 * 
 * The puzzle is as follows:
 * 
 * You're given a four-by-four grid of letters. Form words by making a trail of letters,
 * moving from one tile to any adjacent tile (including diagonally), and identify all
 * valid words formed (by comparison to a word list).
 * 
 * Assumptions unspecified by the puzzle but which I think were intended:
 * - Any given tile can be used only once. Otherwise the problem goes on forever,
 *   barring a much more complicated analysis of the word list.
 * - When you can make the same word via different paths, the word list should
 *   show the word both ways. (If you don't like that, pipe through uniq.)
 * 
 * Frank J. Perricone (hawthorn@foobox.com)
 * 
 */

namespace GridSolver {

  class Program {

    // Some different sizes for testing. The smaller sizes were helpful before I did the "word starts with" 
    // optimization and was getting the bare bones working, but now even the 4x4 works in a couple of minutes.
    //static int GRIDWIDTH = 2;
    //static char[] DefaultGrid = { 'D', 'U', 'E', 'S' };
    //static int GRIDWIDTH = 3;
    //static char[] DefaultGrid = { 'D', 'G', 'H', 'K', 'L', 'P', 'Y', 'E', 'U' };
    //static int GRIDWIDTH = 4;
    //static char[] DefaultGrid = { 'D', 'G', 'H', 'I', 'K', 'L', 'P', 'S', 'Y', 'E', 'U', 'T', 'E', 'O', 'R', 'N' };
    static int GRIDWIDTH = 5;
    static char[] DefaultGrid = { 'D', 'G', 'H', 'I', 'K', 'L', 'P', 'S', 'Y', 'E', 'U', 'T', 'E', 'O', 'R', 'N', 'D', 'G', 'H', 'I', 'K', 'L', 'P', 'S', 'Y' };

    // this one is the array index size
    static int GRIDSIZE = GRIDWIDTH * GRIDWIDTH - 1;


    //////////////////////////////////////////////////////////////////////////
    // Converts a series of positions into a string (lower cased)
    static string PositionsToString(char[] TheGrid, int[] Positions) {
      string result = "";
      foreach (int i in Positions) 
        result += TheGrid[i];
      return result.ToLower();
    }

    //////////////////////////////////////////////////////////////////////////
    // Given an array with a series of positions within the grid,
    // and a presumed already-provided word list, tells if a word
    // has been formed.
    static bool IsAWord(string[] WordList, string AllegedWord)
    {
      return (Array.Exists(WordList, p => p == AllegedWord));
    }

    //////////////////////////////////////////////////////////////////////////
    // This predicate is used in the following function. It returns true
    // if the target consists of the same letters as the source, plus at
    // at least one more. E.g., 
    // StringStartsWith("pre",       "pre") -> false
    // StringStartsWith("prediction","pre") -> true
    // StringStartsWith("proffer",   "pre") -> false
    static bool StringStartsWith(string target, string source)
    {
      if (target.Length <= source.Length) return false;
      if (target.Substring(0, source.Length) == source) return true;
      return false;
    }

    //////////////////////////////////////////////////////////////////////////
    // As an optimization, determine if any words exist which start with
    // a prefix string, so we can bail on the entire recursion if not.
    // For instance, "pre" should say yes ("prefix", "preen", etc.") but
    // "prd" should say no.
    static bool AnyWordsStartWith(string[] WordList, string Prefix)
    {
      return Array.Exists(WordList, p => StringStartsWith(p, Prefix));
    }

    //////////////////////////////////////////////////////////////////////////
    // Returns a position in a given direction, or -1 if that's
    // outside the grid. Directions are:
    //     0 1 2
    //     3 X 4
    //     5 6 7
    static int AdjacentPosition(int Position, int Direction)
    {
      // convert a position to row and column
      int row = Position / GRIDWIDTH;
      int col = Position % GRIDWIDTH;

      // an array is faster than doing the math, and this is
      // inside a frequently-executed function so a good place
      // to throw away a few dozen bytes to gain that speed
      int[] DirectionRowDelta = {-1, -1, -1,  0, 0,  1, 1, 1};
      int[] DirectionColDelta = { -1, 0, 1, -1, 1, -1, 0, 1 };

      // shift
      row += DirectionRowDelta[Direction];
      col += DirectionColDelta[Direction];

      // throw a -1 if we're now outside the grid
      if (row < 0 || row >= GRIDWIDTH || col < 0 || col >= GRIDWIDTH) return -1;

      // convert back to position
      return row * GRIDWIDTH + col;
    }

    //////////////////////////////////////////////////////////////////////////
    // Given a series of positions used so far and a direction,
    // returns the position in that direction, but only if it's
    // a valid position not already present in the list;
    // this is used while recursing to avoid going around in loops
    // and thus having endless recursion. The assumption, which
    // isn't explicitly stated in the problem but is implied by
    // the sample answers, is that you can't go back over a letter
    // repeatedly (e.g., SPURT is a valid solution for the provided
    // grid, but SPURTS is not, since the S already got used once).
    // If that assumption is invalid, a different approach is
    // needed... and the problem becomes vastly more complicated,
    // since any given string of fifty letters is not necessarily
    // the end of your search since maybe there's a fifty-one
    // letter word you haven't found, so you can only solve it
    // through analysis of the word list you've been provided.
    static int AdjacentUnusedPosition(int[] Positions, int Direction) {
      int NewPosition = AdjacentPosition(Positions[Positions.Length-1], Direction);
      if (NewPosition == -1) return -1;
      if (Array.Exists(Positions, p => p == NewPosition)) return -1;
      return NewPosition;
    }

    //////////////////////////////////////////////////////////////////////////
    // For a given list of positions already explored, report it if it's a word,
    // then see if adding one more position will make a word in each possible
    // direction (excluding crossing back over places you've already been,
    // or falling off the grid). This is a function that will recurse extensively,
    // and is where the main work of the program is done.
    static void RecursePosition(int[] Positions, string[] WordList, char[] TheGrid) {

      // First, if this is a word, report it to the output.
      string AllegedWord = PositionsToString(TheGrid, Positions);
      if (IsAWord(WordList, AllegedWord)) Console.WriteLine("  {0}",AllegedWord);
      // Doing this first ensures single-letter words are found (e.g., I)

      // If the grid allows the same word to be created by differing
      // paths, it will be reported each time. For instance, in
      // the supplied grid, LYE can be made two ways (5 8 9 and 5 8 12)
      // and so will be listed twice. I consider this a feature, but
      // if it's considered a bug, you could always pipe the output
      // into uniq! (Or solve it with an internal cache, but that's a
      // much less modular solution, so I haven't done that here.)

      // Next, if the list is already sixteen elements, take an early exit
      // Note: if there are no valid directions, we won't recurse anyway,
      // so this is just an optimization, and the code would work without
      // it (ideally we'd prove this with red-green refactoring).
      if (Positions.Length == TheGrid.Length) return;

      // Another optimization is to abandon a search entirely nothing starts 
      // with the letters we already have.
      if (!AnyWordsStartWith(WordList, AllegedWord)) return;

      // For each possible direction, try adding that direction
      for (int Direction = 0; Direction <= 7; Direction++) {
        int NewPosition = AdjacentUnusedPosition(Positions, Direction);
        if (NewPosition != -1) {
          int NewLength = Positions.Length + 1;
          int[] NewPositions = new int[NewLength];
          for (int i = 0; i < Positions.Length; i++) NewPositions[i] = Positions[i];
          NewPositions[NewLength-1] = NewPosition;
          RecursePosition(NewPositions, WordList, TheGrid);
        }
      }
    }

    //////////////////////////////////////////////////////////////////////////
    static int Main(string[] args)
    {
      char[] TheGrid = DefaultGrid;
      // For simplicity of passing around arrays of positions, I'll be
      // using an array of 16 positions. Row one is positions 0,1,2,3;
      // row two is 4,5,6,7; etc.

      // load a random or specified grid if args say to do so
      if (args.Length >= 1 && args[0] == "random") {
        Random r = new Random();
        for (int i = 0; i <= GRIDSIZE; i++) TheGrid[i] = (char)('A' + r.Next(1, 26));
      }
      else if (args.Length >= GRIDSIZE)
      {
        for (int i = 0; i <= GRIDSIZE; i++) TheGrid[i] = Char.ToUpper(args[i][0]);
      }

      // read the word list
      string[] WordList;
      try
      {
        WordList = System.IO.File.ReadAllLines(@"wordlist.txt");
      }
      catch
      {
        Console.WriteLine("FATAL: Error reading the word list.\n");
        return 1;
      }

      // print the grid
      Console.WriteLine("Solving for all words found in this grid:");
      for (int row = 0; row < GRIDWIDTH; row++)
      {
        Console.Write("  ");
        for (int col = 0; col < GRIDWIDTH; col++)
          Console.Write("+---");
        Console.WriteLine("+");
        Console.Write("  ");
        for (int col = 0; col < GRIDWIDTH; col++)
        {
          Console.Write("| {0} ", TheGrid[row * GRIDWIDTH + col]);
        }
        Console.Write("|\n");
      }
      Console.Write("  ");
      for (int col = 0; col < GRIDWIDTH; col++)
        Console.Write("+---");
      Console.WriteLine("+");

      // print a heading saying these are the solutions
      Console.WriteLine("Words found:");
      
      /*
      // some internal testing; if only Visual Studio did a better job of handling TDD, I'd move these into a test suite to avoid regressions
      
      if (IsAWord(WordList, "aardvark")) Console.WriteLine("Aardvarks exist!");
      if (IsAWord(WordList, "bardvark")) Console.WriteLine("Bardvarks exist?");
      int[] Positions = {0, 1, 2, 3};
      Console.WriteLine(PositionsToString(TheGrid, Positions));
      if (IsAWord(WordList, PositionsToString(TheGrid, Positions))) Console.WriteLine("Row 1 exists");
      Console.WriteLine("Starting from position 5:");
      for (int d = 0; d <= 7; d++) {
        Console.WriteLine(AdjacentPosition(5, d));
      }
      Console.WriteLine("Should be 0 1 2 4 6 8 9 10");
      Console.WriteLine("Starting from position 4:");
      for (int d = 0; d <= 7; d++)
      {
        Console.WriteLine(AdjacentPosition(4, d));
      }
      Console.WriteLine("Should be -1 0 1 -1 5 -1 8 9");
      int[] Positions = { 3, 6, 7 };
      Console.WriteLine("Starting from positions 3 6 7:");
      for (int d = 0; d <= 7; d++)
      {
        Console.WriteLine(AdjacentUnusedPosition(Positions, d));
      }
      Console.WriteLine("Should be 2 -1 -1 -1 -1 10 11 -1");
      if (AnyWordsStartWith(WordList, "pre")) Console.WriteLine("Something starts with pre");
      if (AnyWordsStartWith(WordList, "prd")) Console.WriteLine("Something starts with prd");
      */

      // start the recursion from each of the possible starting positions
      for (int StartPosition = 0; StartPosition <= GRIDSIZE; StartPosition++)
      {
        int[] Positions = new int[1];
        Positions[0] = StartPosition;
        RecursePosition(Positions, WordList, TheGrid);
      }

      return 0;
    }
  }
}

